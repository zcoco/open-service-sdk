package com.hundsun.openservice.sdk.bean;

import java.util.Map;

public class ParameterBean {

    private Map<String, String[]> paraMapMust;

    private Map<String, String[]> paraMapMustNot;

    private String queryStr;

    private String dateFieldName = "timestamp";

    private String[] statFields;

    private String dateHistType;

    private boolean dateHist;

    private Map<String, String> metrics;

    private int from = 0;

    private int size = 10;

    public String[] getStatFields() {
        return statFields;
    }

    public void setStatFields(String[] statFields) {
        this.statFields = statFields;
    }

    public Map<String, String[]> getParaMapMust() {
        return paraMapMust;
    }

    public void setParaMapMust(Map<String, String[]> paraMapMust) {
        this.paraMapMust = paraMapMust;
    }

    public Map<String, String[]> getParaMapMustNot() {
        return paraMapMustNot;
    }

    public void setParaMapMustNot(Map<String, String[]> paraMapMustNot) {
        this.paraMapMustNot = paraMapMustNot;
    }

    public String getDateFieldName() {
        return dateFieldName;
    }

    public void setDateFieldName(String dateFieldName) {
        this.dateFieldName = dateFieldName;
    }

    public String getDateHistType() {
        return dateHistType;
    }

    public void setDateHistType(String dateHistType) {
        this.dateHistType = dateHistType;
    }

    public boolean isDateHist() {
        return dateHist;
    }

    public void setDateHist(boolean dateHist) {
        this.dateHist = dateHist;
    }

    public Map<String, String> getMetrics() {
        return metrics;
    }

    public void setMetrics(Map<String, String> metrics) {
        this.metrics = metrics;
    }

    public String getQueryStr() {
        return queryStr;
    }

    public void setQueryStr(String queryStr) {
        this.queryStr = queryStr;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
